﻿using MultipleViewMVVM.Models;
using MultipleViewMVVM.ViewModels.Commands;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MultipleViewMVVM.ViewModels
{
    public class AddViewModel: ViewModelBase
    {
        //public delegate void ViewModelBaseEnventHandler(object source, EventArgs args);
        //public event ViewModelBaseEnventHandler AddButtonClicked;
        public RelayCommand addViewCheckCommand { get; set; }
        public AddViewModel()
        {
            addViewCheckCommand = new RelayCommand(ExecuteCheckCommand, CanExecuteCheckCommand);
        }

        private bool CanExecuteCheckCommand(object param)
        {
            return true;
        }

        private void ExecuteCheckCommand(object param)
        {
            BocciaPlayer player = param as BocciaPlayer;
            AddToListView(player);
            //var currentViewFromViewModelBase = (ViewModelBase) System.Windows.Application.Current
            //OnAddButtonClicked();
        }

        /*protected virtual void OnAddButtonClicked()
        {
            if (AddButtonClicked != null)
                AddButtonClicked(this, EventArgs.Empty);
        }*/
    }
}
