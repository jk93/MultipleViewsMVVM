﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MultipleViewMVVM.ViewModels
{
    public class GreenViewModel: ViewModelBase
    {
        public Image MyImage
        {
            get
            {
                return new Bitmap(@"Images\great_image");
            }
        }
    }
}
